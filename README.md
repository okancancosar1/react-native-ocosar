# react-native-ocosar

## Getting started

`$ npm install react-native-ocosar --save`

## Usage

```javascript
import OCosaR from "react-native-ocosar";
```

### For Communication with encryption React-native and Asp.Net WebApi

```javascript
https://gist.github.com/OkancanCosar/1919188492b6bdedd96a2ff7e1b929d1
```

### Some Helpers

```javascript
export default {
  capitalizeFirstChar,
  getNow,
  getDate,
  getRandomInt,
  ChangeDateFormat,
  setAsyncStorage,
  getAsyncStorage,
  DATEFORMATS,
  _encrypt,
  _decrypt,
  GStyles,
  height,
  width,
};
```
