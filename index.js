import { NativeModules } from "react-native";

import { GStyles, height, width } from "./src/GStyle";

import {
  capitalizeFirstChar,
  getNow,
  getDate,
  getRandomInt,
  ChangeDateFormat,
  setAsyncStorage,
  getAsyncStorage,
  DATEFORMATS,
} from "./src/CHelper";

const { OCosaR } = NativeModules;

const { _encrypt, _decrypt } = OCosaR;

/**
    
    import OCosaR from "react-native-ocosar";
    
    OCosaR._encrypt("__PLAIN_TEXT__", "OkancanCOŞAR", encriptedText => {
      console.log("ENCRIPTED:: " + encriptedText);

      OCosaR._decrypt(encriptedText, "OkancanCOŞAR", plainText => {
        console.log("DECRIPTED:: " + plainText);
      });
      
    });

 */
export default {
  capitalizeFirstChar,
  getNow,
  getDate,
  getRandomInt,
  ChangeDateFormat,
  setAsyncStorage,
  getAsyncStorage,
  DATEFORMATS,
  _encrypt,
  _decrypt,
  GStyles,
  height,
  width,
};
