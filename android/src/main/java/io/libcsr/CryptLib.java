package io.libcsr;

import android.util.Base64;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class CryptLib {

    private Cipher _cx;
    private byte[] _key, _iv;

    public CryptLib() throws NoSuchAlgorithmException, NoSuchPaddingException {
        _cx = Cipher.getInstance("AES/CBC/PKCS5Padding");
        _key = new byte[32];
        _iv = new byte[16];
    }

    private static String SHA256(String text) throws NoSuchAlgorithmException {
        StringBuilder result = new StringBuilder();
        String resultStr;
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        md.update(text.getBytes(StandardCharsets.UTF_8));
        for (byte b : md.digest()) {
            result.append(String.format("%02x", b));
        }
        if (32 > result.toString().length()) resultStr = result.toString();
        else resultStr = result.toString().substring(0, 32);
        return resultStr;
    }

    public String encrypt(String _plainText, String _key)
            throws InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException, NoSuchAlgorithmException {
        return encryptDecrypt(_plainText, _key, EncryptMode.ENCRYPT);
    }

    public String decrypt(String _encryptedText, String _key)
            throws InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException, NoSuchAlgorithmException {
        return encryptDecrypt(_encryptedText, _key, EncryptMode.DECRYPT);
    }

    private String encryptDecrypt(String _inputText, String _encryptionKey, EncryptMode _mode)
            throws
            InvalidKeyException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException {
        String _out = "";
        _encryptionKey = SHA256(_encryptionKey);
        int len = _encryptionKey.getBytes(StandardCharsets.UTF_8).length;
        if (_encryptionKey.getBytes(StandardCharsets.UTF_8).length > _key.length) len = _key.length;
        String _initVector = "%^<^&RG5sfvF&G!8";
        int ivlen = _initVector.getBytes(StandardCharsets.UTF_8).length;
        if (_initVector.getBytes(StandardCharsets.UTF_8).length > _iv.length) ivlen = _iv.length;

        System.arraycopy(_encryptionKey.getBytes(StandardCharsets.UTF_8), 0, _key, 0, len);
        System.arraycopy(_initVector.getBytes(StandardCharsets.UTF_8), 0, _iv, 0, ivlen);
        SecretKeySpec keySpec = new SecretKeySpec(_key, "AES");
        IvParameterSpec ivSpec = new IvParameterSpec(_iv);
        if (_mode.equals(EncryptMode.ENCRYPT)) {
            // Potentially insecure random numbers on Android 4.3 and older.
            // Read
            // https://android-developers.blogspot.com/2013/08/some-securerandom-thoughts.html
            // for more info.
            _cx.init(Cipher.ENCRYPT_MODE, keySpec, ivSpec);// Initialize this cipher instance
            byte[] results = _cx.doFinal(_inputText.getBytes(StandardCharsets.UTF_8)); // Finish
            // multi-part
            // transformation
            // (encryption)
            _out = Base64.encodeToString(results, Base64.DEFAULT); // ciphertext
            // output
        }
        if (_mode.equals(EncryptMode.DECRYPT)) {
            _cx.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);// Initialize this ipher instance

            byte[] decodedValue = Base64.decode(_inputText.getBytes(),
                    Base64.DEFAULT);
            byte[] decryptedVal = _cx.doFinal(decodedValue); // Finish
            // multi-part
            // transformation
            // (decryption)
            _out = new String(decryptedVal);
        }
        return _out;
    }

    private enum EncryptMode {ENCRYPT, DECRYPT}
}