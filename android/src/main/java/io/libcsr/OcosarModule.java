package io.libcsr;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class OcosarModule extends ReactContextBaseJavaModule {

    private final ReactApplicationContext reactContext;
    private CryptLib _crypt;

    public OcosarModule(ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;

        try {
            _crypt = new CryptLib();
        } catch (Exception ignored) {
        }
    }

    @Override
    public String getName() {
        return "OCosaR";
    }

    @ReactMethod
    public void _encrypt(String PlainText, String Key, Callback callback) {
        try {
            callback.invoke(_crypt.encrypt(PlainText, Key));
        } catch (Exception e) {
            callback.invoke("");
        }
    }

    @ReactMethod
    public void _decrypt(String EncryptedText, String Key, Callback callback) {
        try {
            callback.invoke(_crypt.decrypt(EncryptedText, Key));
        } catch (Exception e) {
            callback.invoke("");
        }
    }

}
