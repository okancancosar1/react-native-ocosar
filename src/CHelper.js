import AsyncStorage from "@react-native-community/async-storage";

import moment from "moment";
import "moment/locale/tr";

const DATEFORMATS = {
  TIMESTAMP: "x",
  READABLE: "ll", // return 15 Oca 2020
  SENDABLE: "L", // return 15.01.2020
  YMD: "YYYYMMDD", // return 20200115
  ONLYDAY: "DD", // return 15
  ONLYYEAR: "YYYY", // return 2020
};

const getNow = () => {
  // now and timestamp
  moment.locale("tr");
  return moment().unix().toString();
};

const getDate = (date = getNow(), format = DATEFORMATS.READABLE) => {
  moment.locale("tr");
  return moment.unix(date).format(format).toString();
};

const ChangeDateFormat = (date, format = DATEFORMATS.READABLE) => {
  moment.locale("tr");
  if (!date) return "";
  return moment(date).format(format).toString();
};

const getRandomInt = max => {
  return Math.floor(Math.random() * max);
};

const capitalizeFirstChar = s => {
  if (typeof s !== "string") return "";
  return s.charAt(0).toUpperCase() + s.slice(1);
};

const setAsyncStorage = async (key, val) => {
  try {
    await AsyncStorage.setItem(`@${key}@`, val);
  } catch (e) {
    return false;
  }
  return true;
};
const getAsyncStorage = async key => {
  try {
    return await AsyncStorage.getItem(`@${key}@`);
  } catch (e) {
    return null;
  }
};

export {
  capitalizeFirstChar,
  getNow,
  getDate,
  getRandomInt,
  ChangeDateFormat,
  setAsyncStorage,
  getAsyncStorage,
  DATEFORMATS,
};
